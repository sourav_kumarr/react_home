/**
 * Created by Sourav on 9/1/2017.
 */

import React, { Component } from 'react';
import {
    ScrollView,
    Text,
    TextInput,
    View,
    Button,
    Alert,
    StyleSheet
} from 'react-native';

const onLoginPress = () => {
  Alert.alert("Login","Logged in successfully");
};


export default class Login extends Component {


    render() {
        return (

                <Image
                    source={require('images/back.jpg')}
                    style={styles.container}>
                <Text
                    style={styles.loginText}>
                    Login
                </Text>
                <TextInput placeholder='Username' />
                <TextInput placeholder='Password' />
                <View style={{margin:7}} />
                <Button
                    onPress={() => onLoginPress()}
                    title="Submit"
                />
                </Image>
        )
    }

}

const styles = StyleSheet.create({
    bigblue: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 30,
    },
    red: {
        color: 'red',
    },
    loginText:{
        fontSize:30
    },
    container: {
        flex: 1,
        width: undefined,
        height: undefined,
        justifyContent: 'center',
        alignItems: 'center',
    }
});
